package edu.towson.cosc435.valis.labsapp.ui.nav

sealed class Routes(val route: String) {
    object SongList : Routes("songlist")
    object NewSong : Routes("newsong")
}