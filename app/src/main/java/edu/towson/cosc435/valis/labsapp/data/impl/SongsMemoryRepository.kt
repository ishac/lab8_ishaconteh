package edu.towson.cosc435.valis.labsapp.data.impl

import edu.towson.cosc435.valis.labsapp.data.ISongsRepository
import edu.towson.cosc435.valis.labsapp.model.Song
import kotlinx.coroutines.delay

class SongsMemoryRepository : ISongsRepository {

    private var _songs = listOf<Song>()

    init {
        _songs = (0..20).map { i ->
            Song(i,"Song $i", "Artist $i", i, i % 3 == 0)
        }
    }

    override suspend fun getSongs(): List<Song> {
        return _songs
    }

    override suspend fun deleteSong(idx: Int) {
        delay(5000)
        _songs = _songs.subList(0, idx) + _songs.subList(idx+1, _songs.size)
    }

    override suspend fun addSong(song: Song) {
//        delay(5000)
        _songs = listOf(song) + _songs
    }

    override suspend fun toggleAwesome(idx: Int) {
        val oldSong = _songs.get(idx)
        val newSong = oldSong.copy(isAwesome = !oldSong.isAwesome)
        _songs = _songs.subList(0, idx) + listOf(newSong) + _songs.subList(idx+1, _songs.size)
    }
}