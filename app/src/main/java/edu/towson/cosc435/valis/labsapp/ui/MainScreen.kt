package edu.towson.cosc435.valis.labsapp.ui

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import edu.towson.cosc435.valis.labsapp.ui.nav.Routes
import edu.towson.cosc435.valis.labsapp.ui.nav.SongsNavGraph

@ExperimentalComposeUiApi
@ExperimentalFoundationApi
@Composable
fun MainScreen() {
    val nav = rememberNavController()
    Scaffold(
        topBar = {
            TopBar()
        },
        bottomBar = {
            BottomBar(nav = nav)
        }
    ) {
        SongsNavGraph(nav)
    }
}

@Composable
private fun TopBar() {
    TopAppBar(
        title = { Text("Songs App") },
    )
}

@Composable
private fun BottomBar(
    nav: NavHostController 
) {
    val backStateEntry = nav.currentBackStackEntryAsState()
    val currentDestination = backStateEntry.value?.destination
    BottomNavigation(
        elevation = 16.dp
    ) {
        BottomNavigationItem(
            selected = currentDestination?.route == Routes.SongList.route,
            onClick = {
                nav.navigate(Routes.SongList.route) {
                    popUpTo(Routes.SongList.route)
                }
            },
            icon = {
                Icon(Icons.Default.Home, "")
            },
            label = {
                Text("SongList")
            }
        )
        BottomNavigationItem(
            selected = currentDestination?.route == Routes.NewSong.route,
            onClick = {
                nav.navigate(Routes.NewSong.route) {
                    launchSingleTop = true
                }
            },
            icon = {
                Icon(Icons.Default.Add, "")
            },
            label = {
                Text("New Song")
            }
        )
    }
}