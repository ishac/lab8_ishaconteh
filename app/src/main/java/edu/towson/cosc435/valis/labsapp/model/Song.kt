package edu.towson.cosc435.valis.labsapp.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "songs")
data class Song(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val name: String,
    val artist: String,
    val track: Int,
    @ColumnInfo(name = "is_awesome")
    val isAwesome: Boolean
) {
}